<!DOCTYPE  html>
<html lang="en">
<?php
include ('Head.php');
?>
<body style>
<!--contiene la barra, el logo y el text buscar -->

<section id="container" class="">
        <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="botton">
                    <i class="icon_menu"></i>
                </div>
            </div>
                <?PHP include("logo.php"); ?>
            
            <div class="nav search-row" id="top_menu">
                <!-- iniciando el texT BUSCAR -->
                <ul class="nav top-menu">
                    <li>
                        <form class="navbar-form">
                            <input class="form-control" placeholder="Buscar..." type="text">
                        </form>
                    </li>
                </ul>   
                <!-- FINALIZANDO el texT BUSCAR -->
            </div>
            <!-- incluyendo el archivo dropdown -->
            <?PHP include ("DropDown.php"); ?>
        </header>
 <!-- incluyendo el muenu izquierdo sin ; medio raro -->
 <?PHP include ("Menu.php")?> 
</section>

<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                     <h3 class="page-header"><i class="fa fa-laptop"></i> PRINCIPAL</h3>
                     
                    <div class="<?PHP echo $alerta; ?>" role="alert">
                        <strong><?PHP echo $mensaje; ?></strong>
                    </div>
                     <ol class="breadcrumb">
                        <?PHP include ("MenuOpcionesConfiguracion.php"); ?>
                    </ol>
                </div>
            </div>
      <!-- ingresadndo el botob de agregar usuarios -->  
        <div class="row">

            <div class="col-lg-12">
                    <div class="row">
                        <section class="panel">
                            <header class="panel-heading">Elija Idioma</header> 
                            <header class="panel-heading">
                                <div class ="panel-body">
                                                                
                                </div> 
                             </header> 

                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th><i class="icon_contacts"></i> IDIOMA </th>
                                    <th><i class="icon_folder"></i> CONTEXTO </th>
                                    
                                </tr>
                                </thead>
                                <?php
                                while ($datosUsuarioIdioma = mysqli_fetch_array($dataIdioma)){
                                 ?>       
                                   <tr>
                                        <td> <?php echo $datosUsuarioIdioma['idioma']; ?></td>
                                        <td>
                                            <a href="#a<?php echo $datosUsuarioIdioma[0]; ?>" role="button" 
                                                class="btn btn-success" data-toggle="modal">
                                                <i class="icon_check_alt2"></i>
                                            </a>

                                        </td>

                                        <!-- formulario para actualizar datos -->
                                    </tr> 
                                     <div id="a<?php echo $datosUsuarioIdioma[0]; ?>" class="modal fade"
                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                             aria-hidden="true">
                                            <form class="form-validate form-horizontal" name="form2"
                                                  action="RegistroDataLanguage.php" method="post">
                                                <input name="usuarioLogin" value="<?php echo $usuario; ?>"
                                                       type="hidden">
                                                <input name="passwordLogin" value="<?php echo $password; ?>"
                                                       type="hidden">
                                                <input type="hidden" name="idIdioma"
                                                       value="<?php echo $datosUsuarioIdioma['idIdioma']; ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">×
                                                            </button>
                                                            <h3 id="myModalLabel" align="center">Elige Idioma</h3>
                                                        </div>

                                                        <div class="modal-body">
                                                            <div class="form-group ">
                                                                <label for="propietario" class="control-label col-lg-2">IDIOMA:</label>
                                                                <select class= "form-control input-lg m-bot15" name="idioma">
                                                                        <option value="Espaniol"> Espanol </option>
                                                                        <option value="Ingles"> Ingles </option>
                                                                        <option value="Portugues">Portugues </option>
                                                                                                                       
                                                                    </select>
                                                            </div>
                                                                                                                       
                                                            <div class="modal-footer">
                                                                <button class="btn btn-default" data-dismiss="modal"
                                                                        aria-hidden="true"><strong>Cerrar</strong>
                                                                </button>
                                                                <button name="update_Data_Idioma" type="submit"
                                                                        class="btn btn-primary"><strong>Actualizar
                                                                        Idioma</strong></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                               <?php }  ?>


                            </table>
                        </div>
                    </div>
                    </section> 
                </div>

            </div><!--/col-->
           
            <!-- statics end -->
        </div>
    </section>
</section>
<!--main content end-->





<?PHP include ("LibraryJs.php"); ?>


<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.
        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datos').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
</script>

<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

</body>
</html>