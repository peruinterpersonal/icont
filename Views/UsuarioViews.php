<!DOCTYPE  html>
<html lang="en">
<?php
include ('Head.php');
?>
<body style>
<!--contiene la barra, el logo y el text buscar -->

<section id="container" class="">
		<header class="header dark-bg">
			<div class="toggle-nav">
				<div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="botton">
					<i class="icon_menu"></i>
				</div>
			</div>
				<?PHP include("logo.php"); ?>
			
			<div class="nav search-row" id="top_menu">
				<!-- iniciando el texT BUSCAR -->
				<ul class="nav top-menu">
					<li>
						<form class="navbar-form">
							<input class="form-control" placeholder="Buscar..." type="text">
						</form>
					</li>
				</ul>	
				<!-- FINALIZANDO el texT BUSCAR -->
			</div>
			<!-- incluyendo el archivo dropdown -->
			<?PHP include ("DropDown.php"); ?>
		</header>
 <!-- incluyendo el muenu izquierdo sin ; medio raro -->
 <?PHP include ("Menu.php")?> 
</section>

<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                     <h3 class="page-header"><i class="fa fa-laptop"></i> PRINCIPAL</h3>
                     
                    <div class="<?PHP echo $alerta; ?>" role="alert">
                        <strong><?PHP echo $mensaje; ?></strong>
                    </div>
                     <ol class="breadcrumb">
                        <?PHP include ("MenuOpcionesConfiguracion.php"); ?>
                    </ol>
                </div>
            </div>
      <!-- ingresadndo el botob de agregar usuarios -->  
        <div class="row">

            <div class="col-lg-12">
                    <div class="row">
                        <section class="panel">
                            <header class="panel-heading">Lista de Usuarios del Sistema</header> 
                            <header class="panel-heading">
                                <div class ="panel-body">
                                    <div align="right">
                                        <button href="#addUser" title="" data-placement="left" data-toggle="modal" 
                                            class="btn btn-primary tooltips" type="button"
                                             data-original-title="Nuevo Usuario">
                                            <span class="fa fa-plus"></span>
                                             AGREGAR NUEVO USUARIO  
                                         </button>
                                     </div>

                                     <!--fomulario agregar usuario -->
                                    <div id="addUser" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"     aria-hidden="true">
                                         <form class="form-validate form-horizontal" name="form2" action="Registros.php"
                                              method="POST" enctype="multipart/form-data">

                                            <!--iniciando la conexion -->
                                            <input  name="usuarioLogin" value="<?PHP echo $usuario; ?>" type="hidden">              
                                            <input  name="passwordLogin" value="<?PHP echo $password; ?>" type="hidden">  
                                            <!-- termina conexion de usuario logueado -->
                                            <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">x
                                                             </button>
                                                             <h3 id=myModalLabel align="center"> Registrar Nuevo Usuario</h3>
                                                         </div>
                                                <!--ARMANDO EL FORMLARIO -->
                                                        <div class="modal-body" style="background-color: rgb(40, 55, 71);" >
                                                            <section class="panel" class="col-lg-6">
                                                                <div>
                                                                    <strong>
                                                                    Agregar Imagen del usuario
                                                                    </strong>
                                                                 </div>    
                                                                     <input id="files" type="file" name="userfile"/>
                                                                     <output id="list-miniatura"> </output>
                                                                     <output id="list-datos"> </output>
                                                            </section>
                                                           
                                                            <label for="nombre" class="control-label col-lg-2"> 
                                                                Nombre:
                                                            </label>
                                                             <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="nombre" name="nombre" minlength="5" type="text" required>
                                                            </div>
                                                             <br><br>    
                                                            
                                                            <label for="Tipo" class="control-label col-lg-2"> 
                                                                Tipo:
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <select class="form-control input-lg m-bot15" name="tipo" >
                                                                    <option value="ADMINISTRADOR">ADMINISTRADOR</option>
                                                                    <option value="VENTAS">VENTAS</option>
                                                                      <option value="VENTAS">AUDITOR</option>
                                                                </select>
                                                            </div>
                                                            <br><br>                 

                                                            <label for="login" class="control-label col-lg-2"> 
                                                                Login:
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <input class="form-control input-lg m-bot15" id="login" name="login" minlength="5" type="text" required>
                                                            </div>
                                                            <br><br>    
                                                            
                                                            <label for="password" class="control-label col-lg-2"> 
                                                                Password:
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <input class="form-control input-lg m-bot15" id="password" name="password" minlength="5" type="text" required>
                                                            </div>
                                                            <br><br>   
                                                          </div>          

                                                          <div class="modal-footer">
                                                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">
                                                                <strong>Cerrar</strong>
                                                            </button>

                                                            <button name="nuevo_usuario" type="submit" class="btn btn-primary" >
                                                                <strong>Registrar</strong>
                                                            </button>
                                                          </div> 
                                                    </div>
                                            </div>
                                    </div>
                                </form>
                                 </div>    
                                </div> 
                             </header> 

                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th><i class="icon_images"></i>IMAGEN</th>
                                    <th><i class="icon_contacts"></i>NOMBRE</th>
                                    <th><i class="icon_folder"></i>TIPO</th>
                                    <th><i class="icon_contacts_alt"></i>LOGIN</th>
                                    <th><i class="icon_key"></i>PASSWORD</th>
                                    <th><i class="icon_cog"></i>ACCIONES</th>
                                </tr>
                                </thead>
                                <?php
                                while ($datosUsuarios= mysqli_fetch_array($allUsuarios)){
                                 ?>       
                                   <tr>
                                        <td><img src="<?php echo $urlViews. $datosUsuarios['foto']?>" heigth ="50px" width="50px"></td>
                                        <td><?php echo $datosUsuarios['nombre'];?></td>
                                        <td><?php echo $datosUsuarios['tipo'];?></td>
                                        <td><?php echo $datosUsuarios['login'];?></td>
                                        <td><?php echo $datosUsuarios['password'];?></td>
                                        <td>
                                            <a href="#a<?php echo $datosUsuarios[0]; ?>" ROLE="button" class="btn btn-success" data-toggle="modal"><i class="icon_check_alt2"></i></a>
                                            <a href="Registros.php?idborrar=<?PHP echo $datosUsuarios[0]; ?>&usuarioLogin=<?PHP  echo $usuario;?>&passwordLogin=<?PHP echo $password;?> " ROLE="button" class="btn btn-danger"><i class="icon_close_alt2"></i></a>
                                            
                                        </td>

                                        <!-- formulario para actualizar datos -->
                                    </tr> 
                                     <div id="a<?php echo $datosUsuarios[0]; ?>" class="modal fade" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form class="form-validate form-horizontal" name="form2"
                                                  action="Registros.php" method="post" enctype="multipart/form-data">
                                                <input name="usuarioLogin" value="<?php echo $usuario; ?>"
                                                       type="hidden">
                                                <input name="passwordLogin" value="<?php echo $password; ?>"
                                                       type="hidden">
                                                <input type="hidden" name="idUsuario"
                                                       value="<?php echo $datosUsuarios['id_usu']; ?>">
                                                <input type="hidden" name="imagen"
                                                       value="<?php echo $datosUsuarios['foto']; ?>">

                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">×
                                                            </button>
                                                            <h3 id="myModalLabel" align="center">Cambiar Informacion del Usuario</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="<?PHP echo $urlViews;
                                                            echo $datosUsuarios['foto']; ?>" width="250" height="250">
                                                            <br><br>
                                                            <section class="panel" class="col-lg-6">
                                                                <div><strong>Cambiar Imagen de usuario</strong></div>
                                                                <?php include("UploadViewImageEdit.php"); ?>
                                                            </section>

                                                            <div class="form-group ">
                                                                <label for="proveedor"
                                                                       class="control-label col-lg-2">Nombre:</label>
                                                                <div class="col-lg-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="text" name="nombre"
                                                                           value="<?php echo $datosUsuarios['nombre']; ?>">
                                                                    <input type="hidden" name="idUsuario"
                                                                           value="<?php echo $datosUsuarios['id_usu']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="responsable"
                                                                       class="control-label col-lg-2">Tipo:</label>
                                                                <div class="col-lg-10">
                                                                    <select class="form-control input-lg m-bot15"
                                                                            name="tipo">
                                                                        <option value="<?php echo $datosUsuarios['tipo']; ?>"><?php echo $datosUsuarios['tipo']; ?></option>
                                                                        <option value="ADMINISTRADOR">
                                                                            ADMINISTRADOR
                                                                        </option>
                                                                        <option value="VENTAS">VENTAS</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="direccion"
                                                                       class="control-label col-lg-2">Login:</label>
                                                                <div class="col-lg-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="text" name="login"
                                                                           value="<?php echo $datosUsuarios['login']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="telefono"
                                                                       class="control-label col-lg-2">Password:</label>
                                                                <div class="col-lg-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="text" name="password"
                                                                           value="<?php echo $datosUsuarios['password']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-default" data-dismiss="modal"
                                                                        aria-hidden="true"><strong>Cerrar</strong>
                                                                </button>
                                                                <button name="update_usuario" type="submit"
                                                                        class="btn btn-primary"><strong>Actualizar  Datos</strong></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                               <?php }  ?>


                            </table>
                        </div>
                    </div>
                    </section> 
                </div>

            </div><!--/col-->
           
            <!-- statics end -->
        </div>
    </section>
</section>
<!--main content end-->





<?PHP include ("LibraryJs.php"); ?>


<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.
        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datos').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
</script>

<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

</body>
</html>